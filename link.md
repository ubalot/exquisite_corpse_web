# Link progetto

## Boilerplate:
- https://github.com/react-boilerplate/react-boilerplate

## Material-ui:
- https://material-ui.com/
- https://material-ui.com/demos/app-bar/
- https://material-ui.com/style/icons/

## Progetto Heroku
- https://dashboard.heroku.com/apps/catch-a-word


# Comandi
start server locally:
```bash
heroku local web
```

Get code from upstream:
```bash
git pull
```
```bash
# se non funziona il precedente
git pull heroku
```

push code to heroku (heroku deploy):
```bash
git add .  # run from project root folder
git commit -a -m "Insert commit description here"
git push heroku master
```

View what failed with heroku build:
```bash
heroku logs
```

View production build on heroku:
```bash
heroku web
```
