const argv = require('./argv');

const port =
  process.env.NODE_ENV !== 'production'
    ? '3000'
    : argv.port || process.env.PORT || '3000';

module.exports = parseInt(port, 10);
