import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

function GhostPaperSheet(props) {
  const { classes, children } = props;

  return (
    <div>
      <Paper className={classes.root} elevation={0}>
        {children}
      </Paper>
    </div>
  );
}

GhostPaperSheet.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.any.isRequired,
};

export default withStyles(styles)(GhostPaperSheet);
