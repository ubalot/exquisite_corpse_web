import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {
  Home as HomeIcon,
  KeyboardArrowLeft as KeyboardArrowLeftIcon,
} from '@material-ui/icons';
import history from '../../utils/history';
import InfoButton from '../InfoButton';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

function ButtonAppBar(props) {
  const { classes, username } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          {history.location.pathname !== '/' && (
            <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
              component={Link}
              to="/"
            >
              <KeyboardArrowLeftIcon />
            </IconButton>
          )}
          <Typography variant="h6" color="inherit" className={classes.grow}>
            Exquisite Corpse
            <InfoButton target="https://en.wikipedia.org/wiki/Exquisite_corpse" />
          </Typography>
          {history.location.pathname.match(/^\/login$/) ? (
            <Button color="inherit" component={Link} to="/">
              <HomeIcon />
            </Button>
          ) : (
            <Button color="inherit" component={Link} to="/login">
              {username || 'Login'}
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
  username: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export default withStyles(styles)(ButtonAppBar);
