import React from 'react';
import PropTypes from 'prop-types';
import { IconButton } from '@material-ui/core';
import { Info as InfoIcon } from '@material-ui/icons';

function InfoButton(props) {
  const { target } = props;

  return (
    <a rel="noopener noreferrer" href={target} target="_blank">
      <IconButton>
        <InfoIcon />
      </IconButton>
    </a>
  );
}

InfoButton.propTypes = {
  target: PropTypes.string,
};

export default InfoButton;
