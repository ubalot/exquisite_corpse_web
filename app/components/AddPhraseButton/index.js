import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Fab,
  TextField,
  Tooltip,
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';

const styles = theme => ({
  fab: {
    // margin: theme.spacing.unit,
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});

class FloatingActionButtons extends React.Component {
  state = {
    open: false,
    word: '',
  };

  handleWordChange = event => {
    this.setState({ word: event.target.value });
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  onSubmit = () => {
    const { onSubmit, userId, storyId } = this.props;
    const { word } = this.state;

    onSubmit(word, storyId, userId);
    this.handleClose();
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <div>
        <Tooltip title="Add a phrase" aria-label="Add a phrase">
          <Fab
            color="primary"
            aria-label="Add"
            className={classes.fab}
            onClick={this.handleClickOpen}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            Add a new phrase to this story
          </DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="new-phrase"
              label="Phrase"
              type="text"
              fullWidth
              onChange={this.handleWordChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.onSubmit} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

FloatingActionButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  onSubmit: PropTypes.func,
  userId: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  storyId: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
};

export default withStyles(styles)(FloatingActionButtons);
