import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
// import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import CardMedia from '@material-ui/core/CardMedia';
// import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
};

function MediaCard(props) {
  const {
    actualPhrasesCount,
    classes,
    creator,
    onStoryClick,
    phrasesLimit,
    title,
    wordsPerPhraseLimit,
  } = props;

  return (
    <Card className={classes.card}>
      <CardActionArea onClick={onStoryClick}>
        {/* <CardMedia
          className={classes.media}
          image="/static/images/cards/contemplative-reptile.jpg"
          title="Contemplative Reptile"
        /> */}
        <CardContent>
          <Typography gutterBottom variant="h5">
            {title}
          </Typography>
          <Typography variant="body1">Creator: {creator}</Typography>
          <Typography variant="body1">
            Phrases count: {actualPhrasesCount}
          </Typography>
          <Typography variant="body1">
            Max number of phrases: {phrasesLimit}
          </Typography>
          <Typography variant="body1">
            Max number of words per phrase: {wordsPerPhraseLimit}
          </Typography>
        </CardContent>
      </CardActionArea>
      {/* <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions> */}
    </Card>
  );
}

MediaCard.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string,
  creator: PropTypes.string,
  actualPhrasesCount: PropTypes.number,
  wordsPerPhraseLimit: PropTypes.number,
  phrasesLimit: PropTypes.number,
  onStoryClick: PropTypes.func,
};

export default withStyles(styles)(MediaCard);
