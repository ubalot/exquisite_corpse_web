import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
// import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    minWidth: 275,
  },
  pos: {
    marginBottom: 12,
  },
};

function SimpleCard(props) {
  const { classes, words, storyTitle } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography
          // color="textSecondary"
          gutterBottom
          variant="h4"
        >
          {storyTitle}
        </Typography>
        {words.map(word => (
          <Typography key={word.id} variant="body1">
            {word.text}
          </Typography>
        ))}
      </CardContent>
      {/* <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions> */}
    </Card>
  );
}

SimpleCard.propTypes = {
  classes: PropTypes.object.isRequired,
  words: PropTypes.array,
  storyTitle: PropTypes.string,
};

export default withStyles(styles)(SimpleCard);
