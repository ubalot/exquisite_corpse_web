import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Fab,
  TextField,
  Tooltip,
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';

const PHRASES_MIN_LIMIT = 6;

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit,
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});

class FloatingActionButtons extends React.Component {
  state = {
    open: false,
    title: '',
    phrasesLimit: PHRASES_MIN_LIMIT,
    wordsPerPhraseLimit: 1,
  };

  handleStoryTitleChange = event => {
    this.setState({ title: event.target.value });
  };

  handleStoryPhrasesLimitChange = event => {
    this.setState({ phrasesLimit: event.target.value });
  };

  handleStoryWordsPerPhrasesLimitChange = event => {
    this.setState({ wordsPerPhraseLimit: event.target.value });
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  onSubmit = () => {
    const { onSubmit, userId } = this.props;
    const { title, phrasesLimit, wordsPerPhraseLimit } = this.state;

    onSubmit(title, phrasesLimit, wordsPerPhraseLimit, userId);
    this.handleClose();
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <div>
        <Tooltip title="Create new story" aria-label="Create new story">
          <Fab
            color="primary"
            aria-label="Add"
            className={classes.fab}
            onClick={this.handleClickOpen}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">New story</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Give a title to your story and define how many words it is going
              to contain.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="new-story-title"
              label="Story title"
              type="text"
              fullWidth
              onChange={this.handleStoryTitleChange}
            />
            <TextField
              margin="dense"
              id="story-words-max"
              label="Max number of phrases"
              type="number"
              InputProps={{ inputProps: { min: PHRASES_MIN_LIMIT } }}
              defaultValue={6}
              fullWidth
              onChange={this.handleStoryPhrasesLimitChange}
            />
            <TextField
              margin="dense"
              id="story-words-per-story-max"
              label="Max number of words per phrase"
              type="number"
              InputProps={{ inputProps: { min: 1 } }}
              defaultValue={1}
              fullWidth
              onChange={this.handleStoryWordsPerPhrasesLimitChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.onSubmit} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

FloatingActionButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  onSubmit: PropTypes.func,
  userId: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]).isRequired,
};

export default withStyles(styles)(FloatingActionButtons);
