/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest } from 'redux-saga/effects';

import request from 'utils/request';
import { API } from '../App/constants';
import {
  storiesLoadingError,
  storiesLoaded,
  storyCreated,
  storyCreationError,
  fetchStories,
} from './actions';
import { LOAD_STORIES_REQUEST, CREATE_STORY_REQUEST } from './constants';

const getPostOptions = data => ({
  method: 'POST',
  mode: 'cors',
  cache: 'no-cache',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
});

function* handleFetchStories() {
  const requestURL = `${API}/stories`;
  try {
    const stories = yield call(request, requestURL);
    yield put(
      storiesLoaded(
        stories.sort(
          (a, b) => (a.title.toLowerCase() > b.title.toLowerCase() ? 1 : -1),
        ),
      ),
    );
  } catch (error) {
    yield put(storiesLoadingError(error.message));
  }
}

function* createStory(action) {
  const { title, phrasesLimit, wordsPerPhraseLimit, userId } = action;
  const requestURL = `${API}/stories`;
  try {
    const story = yield call(
      request,
      requestURL,
      getPostOptions({
        title,
        phrases_limit: phrasesLimit,
        words_per_phrase_limit: wordsPerPhraseLimit,
        user_id: userId,
      }),
    );
    yield put(storyCreated(story));
    yield put(fetchStories());
  } catch (error) {
    yield put(storyCreationError(error.message));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* fetchStoriesData() {
  yield takeLatest(LOAD_STORIES_REQUEST, handleFetchStories);
  yield takeLatest(CREATE_STORY_REQUEST, createStory);
}
