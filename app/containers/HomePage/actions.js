/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  CHANGE_USERNAME,
  LOAD_STORIES_SUCCESS,
  LOAD_STORIES_ERROR,
  LOAD_STORIES_REQUEST,
  CREATE_STORY_REQUEST,
  CREATE_STORY_SUCCESS,
  CREATE_STORY_ERROR,
} from './constants';
// import { LOAD_STORIES_REQUEST } from '../App/constants';

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(name) {
  return {
    type: CHANGE_USERNAME,
    name,
  };
}

export function fetchStories() {
  return {
    type: LOAD_STORIES_REQUEST,
  };
}

export function storiesLoaded(stories) {
  return {
    type: LOAD_STORIES_SUCCESS,
    stories,
  };
}

export function storiesLoadingError(error) {
  return {
    type: LOAD_STORIES_ERROR,
    error,
  };
}

export function createStory(title, phrasesLimit, wordsPerPhraseLimit, userId) {
  return {
    type: CREATE_STORY_REQUEST,
    title,
    phrasesLimit,
    wordsPerPhraseLimit,
    userId,
  };
}

export function storyCreated(story) {
  return {
    type: CREATE_STORY_SUCCESS,
    story,
  };
}

export function storyCreationError(error) {
  return {
    type: CREATE_STORY_ERROR,
    error,
  };
}
