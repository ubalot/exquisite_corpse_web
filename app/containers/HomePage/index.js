/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import GhostPaperSheet from 'components/Paper';
import GuttersGrid from 'components/GridList';
import Snackbar from 'components/Snackbar';
import MediaCard from 'components/StoryCard';
import { Typography } from '@material-ui/core';
import reducer from './reducer';
import saga from './saga';
import { fetchStories, createStory } from './actions';
import { makeSelectStories, makeSelectError } from './selectors';
import { makeSelectUserId } from '../App/selectors';
import AddStoryButton from '../../components/AddStoryButton';

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.PureComponent {
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    this.props.fetchStories();
  }

  render() {
    const { error, history, stories, onCreateStory, userId } = this.props;

    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application homepage"
          />
        </Helmet>
        <div>
          <GhostPaperSheet>
            <GuttersGrid>
              {stories.length > 0 ? (
                stories.map(story => (
                  <GhostPaperSheet key={story.id}>
                    <MediaCard
                      key={story.title}
                      title={story.title}
                      creator={story.author.username}
                      phrasesLimit={story.phrases_limit}
                      actualPhrasesCount={story.actual_phrases_count}
                      wordsPerPhraseLimit={story.words_per_phrase_limit}
                      onStoryClick={() => history.push(`/stories/${story.id}`)}
                    />
                  </GhostPaperSheet>
                ))
              ) : (
                <Typography>
                  No stories until now...create a new one yourself!
                </Typography>
              )}
            </GuttersGrid>
          </GhostPaperSheet>
          <AddStoryButton key="add" onSubmit={onCreateStory} userId={userId} />
        </div>
        {error && <Snackbar message={error} />}
      </article>
    );
  }
}

HomePage.propTypes = {
  history: PropTypes.object,
  stories: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  fetchStories: PropTypes.func,
  onCreateStory: PropTypes.func,
  userId: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export function mapDispatchToProps(dispatch) {
  return {
    fetchStories: () => dispatch(fetchStories()),
    onCreateStory: (title, phrasesLimit, wordsPerPhraseLimit, userId) =>
      dispatch(createStory(title, phrasesLimit, wordsPerPhraseLimit, userId)),
  };
}

const mapStateToProps = createStructuredSelector({
  stories: makeSelectStories(),
  userId: makeSelectUserId(),
  error: makeSelectError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
