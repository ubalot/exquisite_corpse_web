/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';

export const GET_STORIES_DATA = 'exquisite-corpse/Home/GET_STORIES_DATA';
// export const FETCH_STORIES = 'exquisite-corpse/Home/FETCH_STORIES';

export const LOAD_STORIES_REQUEST = 'exquisite_corpse/App/LOAD_STORIES_REQUEST';
export const LOAD_STORIES_SUCCESS = 'exquisite_corpse/App/LOAD_STORIES_SUCCESS';
export const LOAD_STORIES_ERROR = 'exquisite_corpse/App/LOAD_STORIES_ERROR';

export const CREATE_STORY_REQUEST = 'exquisite_corpse/App/CREATE_STORY_REQUEST';
export const CREATE_STORY_SUCCESS = 'exquisite_corpse/App/CREATE_STORY_SUCCESS';
export const CREATE_STORY_ERROR = 'exquisite_corpse/App/CREATE_STORY_ERROR';
