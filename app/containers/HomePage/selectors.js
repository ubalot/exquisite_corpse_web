/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.get('home', initialState);

const makeSelectUsername = () =>
  createSelector(selectHome, homeState => homeState.get('username'));

const makeSelectStories = () =>
  createSelector(selectHome, homeState => homeState.get('stories'));

const makeSelectError = () =>
  createSelector(selectHome, homeState => homeState.get('error'));

export { selectHome, makeSelectUsername, makeSelectStories, makeSelectError };
