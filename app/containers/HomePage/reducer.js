/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import {
  // CHANGE_USERNAME,
  LOAD_STORIES_SUCCESS,
  LOAD_STORIES_ERROR,
  CREATE_STORY_SUCCESS,
  CREATE_STORY_ERROR,
} from './constants';

// The initial state of the App
export const initialState = fromJS({
  stories: [],
  error: false,
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    // case CHANGE_USERNAME:
    //   // Delete prefixed '@' from the github username
    //   return state.set('username', action.name.replace(/@/gi, ''));
    case LOAD_STORIES_SUCCESS:
      return state.set('stories', action.stories).set('error', false);
    case LOAD_STORIES_ERROR:
      return state.set('stories', []).set('error', action.error);
    case CREATE_STORY_SUCCESS:
      return state
        .set('stories', state.stories.concat(action.story))
        .set('error', false);
    case CREATE_STORY_ERROR:
      return state.set('stories', []).set('error', action.error);
    default:
      return state;
  }
}

export default homeReducer;
