export const SEND_LOGIN_DATA_REQUEST =
  'exquisite-corpse/Login/SEND_LOGIN_DATA_REQUEST';
export const SEND_LOGIN_DATA_SUCCEDED =
  'exquisite-corpse/Login/SEND_LOGIN_DATA_SUCCEDED';
export const SEND_LOGIN_DATA_FAILED =
  'exquisite-corpse/Login/SEND_LOGIN_DATA_FAILED';

export const SEND_SIGNUP_DATA_REQUEST =
  'exquisite-corpse/Login/SEND_SIGNUP_DATA_REQUEST';
export const SEND_SIGNUP_DATA_SUCCEDED =
  'exquisite-corpse/Login/SEND_SIGNUP_DATA_SUCCEDED';
export const SEND_SIGNUP_DATA_FAILED =
  'exquisite-corpse/Login/SEND_SIGNUP_DATA_FAILED';

export const LOGOUT = 'exquisite-corpse/Login/LOGOUT';
