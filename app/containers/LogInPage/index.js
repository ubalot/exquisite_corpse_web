/*
 * LogInPage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import PaperSheet from 'components/Paper';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import { sendLoginData, sendSignupData, logout } from './actions';
import {
  makeSelectLogged,
  makeSelectLoginError,
  makeSelectSignupError,
} from './selectors';
import { makeSelectUsername } from '../App/selectors';

const styles = theme => ({
  card: {
    minWidth: 275,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
});

/* eslint-disable react/prefer-stateless-function */
export class LogInPage extends React.Component {
  state = {
    loginEmail: '',
    loginUsername: '',
    loginPassword: '',
    signupEmail: '',
    signupUsername: '',
    signupPassword: '',
  };

  onChangeLoginEmail = event => {
    this.setState({ loginEmail: event.target.value });
  };

  onChangeLoginUsername = event => {
    this.setState({ loginUsername: event.target.value });
  };

  onChangeLoginPassword = event => {
    this.setState({ loginPassword: event.target.value });
  };

  onLoginSubmit = () => {
    const { onSendLoginData } = this.props;
    const { loginEmail, loginUsername, loginPassword } = this.state;
    onSendLoginData(loginEmail, loginUsername, loginPassword);
  };

  onLoginReset = () => {
    this.setState({
      loginEmail: '',
      loginUsername: '',
      loginPassword: '',
    });
  };

  onChangeSignupEmail = event => {
    this.setState({ signupEmail: event.target.value });
  };

  onChangeSignupUsername = event => {
    this.setState({ signupUsername: event.target.value });
  };

  onChangeSignupPassword = event => {
    this.setState({ signupPassword: event.target.value });
  };

  onSignupSubmit = () => {
    const { onSendSignupData } = this.props;
    const { signupEmail, signupUsername, signupPassword } = this.state;
    onSendSignupData(signupEmail, signupUsername, signupPassword);
  };

  onSignupReset = () => {
    this.setState({
      signupEmail: '',
      signupUsername: '',
      signupPassword: '',
    });
  };

  onLogoutSubmit = () => {
    const { onLogout } = this.props;

    onLogout();
  };

  render() {
    const { classes, logged, username, loginError, signupError } = this.props;
    const {
      loginEmail,
      loginUsername,
      loginPassword,
      signupEmail,
      signupUsername,
      signupPassword,
    } = this.state;

    const errorAboutLabel = (error, label) =>
      error && error.toLowerCase().includes(label) ? error : false;

    const loginEmailError = errorAboutLabel(loginError, 'email');
    const loginUsernameError = errorAboutLabel(loginError, 'username');
    const loginPasswordError = errorAboutLabel(loginError, 'password');

    const signupEmailError = errorAboutLabel(signupError, 'email');
    const signupUsernameError = errorAboutLabel(signupError, 'username');
    const signupPasswordError = errorAboutLabel(signupError, 'password');

    return (
      <article>
        <Helmet>
          <title>LogIn Page</title>
          <meta
            name="description"
            content="Login page for Exquisite Corpse app."
          />
        </Helmet>
        <div>
          {logged ? (
            <PaperSheet>
              <Card className={classes.card}>
                <CardContent>
                  <Typography
                    className={classes.title}
                    color="textSecondary"
                    gutterBottom
                  >
                    Current user info
                  </Typography>
                  <Typography>Username: {username}</Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" onClick={this.onLogoutSubmit}>
                    Logout
                  </Button>
                </CardActions>
              </Card>
            </PaperSheet>
          ) : (
            <div>
              {/* Login form */}
              <PaperSheet>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography
                      className={classes.title}
                      color="textSecondary"
                      gutterBottom
                    >
                      Log In
                    </Typography>
                    <form
                      className={classes.container}
                      noValidate
                      autoComplete="off"
                    >
                      <TextField
                        id="login-email"
                        label="Email"
                        className={classes.textField}
                        type="email"
                        value={loginEmail}
                        onChange={this.onChangeLoginEmail}
                        margin="normal"
                        error={!!loginEmailError}
                        helperText={loginEmailError}
                      />
                      <TextField
                        id="login-username"
                        label="Name"
                        className={classes.textField}
                        type="text"
                        value={loginUsername}
                        onChange={this.onChangeLoginUsername}
                        margin="normal"
                        error={!!loginUsernameError}
                        helperText={loginUsernameError}
                      />
                      <TextField
                        id="login-password"
                        label="Password"
                        className={classes.textField}
                        type="password"
                        autoComplete="current-password"
                        margin="normal"
                        value={loginPassword}
                        onChange={this.onChangeLoginPassword}
                        error={!!loginPasswordError}
                        helperText={loginPasswordError}
                      />
                    </form>
                  </CardContent>
                  <CardActions>
                    <Button size="small" onClick={this.onLoginSubmit}>
                      OK
                    </Button>
                    <Button size="small" onClick={this.onLoginReset}>
                      Clear
                    </Button>
                  </CardActions>
                </Card>
              </PaperSheet>

              {/* Signup form */}
              <PaperSheet>
                <Card className={classes.card}>
                  <CardContent>
                    <Typography
                      className={classes.title}
                      color="textSecondary"
                      gutterBottom
                    >
                      Sign Up
                    </Typography>
                    <form
                      className={classes.container}
                      noValidate
                      autoComplete="off"
                    >
                      <TextField
                        id="signup-email"
                        label="Email"
                        className={classes.textField}
                        type="email"
                        value={signupEmail}
                        onChange={this.onChangeSignupEmail}
                        margin="normal"
                        error={!!signupEmailError}
                        helperText={signupEmailError}
                      />
                      <TextField
                        id="signup-username"
                        label="Name"
                        className={classes.textField}
                        type="text"
                        value={signupUsername}
                        onChange={this.onChangeSignupUsername}
                        margin="normal"
                        error={!!signupUsernameError}
                        helperText={signupUsernameError}
                      />
                      <TextField
                        id="signup-password"
                        label="Password"
                        className={classes.textField}
                        type="password"
                        autoComplete="current-password"
                        margin="normal"
                        value={signupPassword}
                        onChange={this.onChangeSignupPassword}
                        error={!!signupPasswordError}
                        helperText={signupPasswordError}
                      />
                    </form>
                  </CardContent>
                  <CardActions>
                    <Button size="small" onClick={this.onSignupSubmit}>
                      OK
                    </Button>
                    <Button size="small" onClick={this.onSignupReset}>
                      Clear
                    </Button>
                  </CardActions>
                </Card>
              </PaperSheet>
            </div>
          )}
        </div>
      </article>
    );
  }
}

LogInPage.propTypes = {
  classes: PropTypes.object.isRequired,
  onSendLoginData: PropTypes.func,
  onSendSignupData: PropTypes.func,
  onLogout: PropTypes.func,
  logged: PropTypes.bool,
  username: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  loginError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  signupError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export function mapDispatchToProps(dispatch) {
  return {
    onSendLoginData: (email, username, password) =>
      dispatch(sendLoginData(email, username, password)),
    onSendSignupData: (email, username, password) =>
      dispatch(sendSignupData(email, username, password)),
    onLogout: () => dispatch(logout()),
  };
}

const mapStateToProps = createStructuredSelector({
  logged: makeSelectLogged(),
  username: makeSelectUsername(),
  loginError: makeSelectLoginError(),
  signupError: makeSelectSignupError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(withStyles(styles)(LogInPage));
