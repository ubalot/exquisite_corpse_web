import { takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { SEND_LOGIN_DATA_REQUEST, SEND_SIGNUP_DATA_REQUEST } from './constants';
import { API } from '../App/constants';
import {
  signupFailed,
  signupSucceded,
  loginFailed,
  loginSucceded,
} from './actions';

const getPostOptions = data => ({
  method: 'POST',
  mode: 'cors',
  cache: 'no-cache',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
});

export function* loginDataRequest(action) {
  const { email, username, password } = action;
  const requestUrl = `${API}/users/login`;
  try {
    const response = yield call(
      request,
      requestUrl,
      getPostOptions({ email, username, password }),
    );
    if (response.data) {
      const { user } = response.data;
      yield put(loginSucceded(user.username, user.id));
    } else {
      yield put(loginFailed(response.message));
    }
  } catch (error) {
    yield put(loginFailed(error.message));
  }
}

export function* signUpDataRequest(action) {
  const { email, username, password } = action;
  const requestUrl = `${API}/users`;
  try {
    const response = yield call(
      request,
      requestUrl,
      getPostOptions({ email, username, password }),
    );
    if (response.data) {
      const { user } = response.data;
      yield put(signupSucceded(user.username, user.id));
    } else {
      yield put(signupFailed(response.message));
    }
  } catch (error) {
    yield put(signupFailed(error.message));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* sendLoginData() {
  yield takeLatest(SEND_LOGIN_DATA_REQUEST, loginDataRequest);
  yield takeLatest(SEND_SIGNUP_DATA_REQUEST, signUpDataRequest);
}
