import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.get('login', initialState);

const makeSelectLogged = () =>
  createSelector(selectLogin, loginState => loginState.get('logged'));

const makeSelectLoginError = () =>
  createSelector(selectLogin, loginState =>
    loginState.getIn(['error', 'login']),
  );

const makeSelectSignupError = () =>
  createSelector(selectLogin, loginState =>
    loginState.getIn(['error', 'signup']),
  );

export {
  selectLogin,
  makeSelectLogged,
  makeSelectLoginError,
  makeSelectSignupError,
};
