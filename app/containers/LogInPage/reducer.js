/*
 * LoginReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';
import {
  SEND_LOGIN_DATA_SUCCEDED,
  SEND_LOGIN_DATA_FAILED,
  SEND_SIGNUP_DATA_SUCCEDED,
  SEND_SIGNUP_DATA_FAILED,
  LOGOUT,
} from './constants';

// The initial state of the App
export const initialState = fromJS({
  logged: !!localStorage.getItem('username'),
  error: {
    login: false,
    signup: false,
  },
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case SEND_SIGNUP_DATA_SUCCEDED:
    case SEND_LOGIN_DATA_SUCCEDED:
      return state
        .set('logged', true)
        .setIn(['error', 'login'], false)
        .setIn(['error', 'signup'], false);
    case SEND_SIGNUP_DATA_FAILED:
      return state
        .set('logged', false)
        .setIn(['error', 'login'], false)
        .setIn(['error', 'signup'], action.error);
    case SEND_LOGIN_DATA_FAILED:
      return state
        .set('logged', false)
        .setIn(['error', 'login'], action.error)
        .setIn(['error', 'signup'], false);
    case LOGOUT:
      localStorage.clear();
      return state
        .set('logged', false)
        .setIn(['error', 'login'], false)
        .setIn(['error', 'signup'], false);
    default:
      return state;
  }
}

export default homeReducer;
