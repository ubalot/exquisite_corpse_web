import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  card: {
    minWidth: 275,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
});

class SimpleCard extends React.Component {
  state = {
    username: '',
    password: '',
  };

  submit = () => {
    // console.log(this.state);
    const { onSubmit } = this.props;
    // console.log('onsubmit', onsubmit);
    onSubmit(this.state);
  };

  handleUsernameChange = event => {
    const { value } = event.target;
    this.setState({ username: value });
  };

  handlePasswordChange = event => {
    const { value } = event.target;
    this.setState({ password: value });
  };

  clear = () => {
    this.setState({
      username: '',
      password: '',
    });
  };

  render() {
    // const { classes, title, onSubmit } = this.props;
    const { classes, title } = this.props;
    const { password, username } = this.state;

    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            {title}
          </Typography>
          <form className={classes.container} noValidate autoComplete="off">
            <TextField
              id="standard-name"
              label="Name"
              className={classes.textField}
              value={username}
              onChange={this.handleUsernameChange}
              margin="normal"
            />
            <TextField
              id="standard-password-input"
              label="Password"
              className={classes.textField}
              type="password"
              autoComplete="current-password"
              margin="normal"
              onChange={this.handlePasswordChange}
              value={password}
            />
          </form>
        </CardContent>
        <CardActions>
          <Button size="small" onClick={this.submit}>
            OK
          </Button>
          <Button size="small" onClick={this.clear}>
            Clear
          </Button>
        </CardActions>
      </Card>
    );
  }
}

SimpleCard.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func,
};

export default withStyles(styles)(SimpleCard);
