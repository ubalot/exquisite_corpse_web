import {
  SEND_LOGIN_DATA_REQUEST,
  SEND_SIGNUP_DATA_REQUEST,
  LOGOUT,
  SEND_LOGIN_DATA_SUCCEDED,
  SEND_LOGIN_DATA_FAILED,
  SEND_SIGNUP_DATA_SUCCEDED,
  SEND_SIGNUP_DATA_FAILED,
} from './constants';

export function sendLoginData(email, username, password) {
  return {
    type: SEND_LOGIN_DATA_REQUEST,
    email,
    username,
    password,
  };
}

export function loginSucceded(username, id) {
  return {
    type: SEND_LOGIN_DATA_SUCCEDED,
    username,
    id,
  };
}

export function loginFailed(error) {
  return {
    type: SEND_LOGIN_DATA_FAILED,
    error,
  };
}

export function sendSignupData(email, username, password) {
  return {
    type: SEND_SIGNUP_DATA_REQUEST,
    email,
    username,
    password,
  };
}

export function signupSucceded(username, id) {
  return {
    type: SEND_SIGNUP_DATA_SUCCEDED,
    username,
    id,
  };
}

export function signupFailed(error) {
  return {
    type: SEND_SIGNUP_DATA_FAILED,
    error,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}
