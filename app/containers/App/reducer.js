/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';
import {
  SEND_SIGNUP_DATA_SUCCEDED,
  SEND_LOGIN_DATA_SUCCEDED,
  SEND_SIGNUP_DATA_FAILED,
  SEND_LOGIN_DATA_FAILED,
} from '../LogInPage/constants';

// The initial state of the App
const initialState = fromJS({
  userData: {
    username: localStorage.getItem('username') || false,
    id: Number(localStorage.getItem('user_id')) || false,
  },
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case SEND_SIGNUP_DATA_SUCCEDED:
    case SEND_LOGIN_DATA_SUCCEDED:
      localStorage.setItem('username', action.username);
      localStorage.setItem('user_id', action.id);
      return state
        .setIn(['userData', 'id'], action.id)
        .setIn(['userData', 'username'], action.username);
    case SEND_SIGNUP_DATA_FAILED:
    case SEND_LOGIN_DATA_FAILED:
      return state
        .setIn(['userData', 'id'], false)
        .setIn(['userData', 'username'], false);
    default:
      return state;
  }
}

export default appReducer;
