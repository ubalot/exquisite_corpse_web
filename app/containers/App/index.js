/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { deepOrange } from '@material-ui/core/colors';
import HomePage from 'containers/HomePage/Loadable';
import LogInPage from 'containers/LogInPage/Loadable';
import ButtonAppBar from '../../components/AppBar';
import StoryPage from '../StoryPage';
import { makeSelectUsername } from './selectors';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1e88e5',
    },
    secondary: {
      main: '#43a047',
    },
  },
  status: {
    danger: deepOrange,
  },
  typography: {
    useNextVariants: true,
  },
});

function App(props) {
  const { username } = props;

  return (
    <MuiThemeProvider theme={theme}>
      <Helmet
        titleTemplate="%s - Exquisite Corpse"
        defaultTitle="Exquisite Corpse"
      >
        <meta
          name="description"
          content="An application for Exquisite Corpse game"
        />
      </Helmet>
      <ButtonAppBar username={username} />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/login" component={LogInPage} />
        <Route path="/stories/:storyId" component={StoryPage} />
      </Switch>
    </MuiThemeProvider>
  );
}

App.propTypes = {
  username: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

const mapStateToProps = createStructuredSelector({
  username: makeSelectUsername(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withRouter,
  withConnect,
)(App);
