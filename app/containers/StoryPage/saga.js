import { takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { API } from '../App/constants';
import {
  ADD_WORD_TO_STORY_REQUEST,
  FETCH_STORY_DATA_REQUEST,
} from './constants';
import {
  wordCreationSuccess,
  wordCreationFailed,
  fetchStorySucceded,
  fetchStoryFailed,
  fetchStoryData,
} from './actions';

const getPostOptions = data => ({
  method: 'POST',
  mode: 'cors',
  cache: 'no-cache',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
});

export function* storyDataRequest(action) {
  const { storyId } = action;
  const requestUrl = `${API}/stories/${storyId}`;
  try {
    const response = yield call(request, requestUrl);
    yield put(fetchStorySucceded(response));
  } catch (error) {
    yield put(fetchStoryFailed(error.message));
  }
}

export function* addWordToStory(action) {
  const { text, userId, storyId } = action;
  const requestUrl = `${API}/words`;
  try {
    const response = yield call(
      request,
      requestUrl,
      getPostOptions({ text, user_id: userId, story_id: storyId }),
    );
    if (response.message) {
      yield put(wordCreationFailed(response.message));
    } else {
      yield put(wordCreationSuccess(response));
      yield put(fetchStoryData(storyId));
    }
  } catch (error) {
    yield put(wordCreationFailed(error.message));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* getStoryDataSaga() {
  yield takeLatest(FETCH_STORY_DATA_REQUEST, storyDataRequest);
  yield takeLatest(ADD_WORD_TO_STORY_REQUEST, addWordToStory);
}
