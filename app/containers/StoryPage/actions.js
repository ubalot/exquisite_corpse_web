import {
  ADD_WORD_TO_STORY_REQUEST,
  ADD_WORD_TO_STORY_SUCCEDED,
  ADD_WORD_TO_STORY_FAILED,
  FETCH_STORY_DATA_REQUEST,
  FETCH_STORY_DATA_SUCCEDED,
  FETCH_STORY_DATA_FAILED,
} from './constants';

export function fetchStoryData(storyId) {
  return {
    type: FETCH_STORY_DATA_REQUEST,
    storyId,
  };
}

export function fetchStorySucceded(story) {
  return {
    type: FETCH_STORY_DATA_SUCCEDED,
    story,
  };
}

export function fetchStoryFailed(error) {
  return {
    type: FETCH_STORY_DATA_FAILED,
    error,
  };
}

export function addWordToStory(text, storyId, userId) {
  return {
    type: ADD_WORD_TO_STORY_REQUEST,
    text,
    userId,
    storyId,
  };
}

export function wordCreationSuccess(story) {
  return {
    type: ADD_WORD_TO_STORY_SUCCEDED,
    story,
  };
}

export function wordCreationFailed(error) {
  return {
    type: ADD_WORD_TO_STORY_FAILED,
    error,
  };
}
