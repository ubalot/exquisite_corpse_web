import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectStory = state => state.get('story', initialState);

const makeSelectStoryData = () =>
  createSelector(selectStory, storyState =>
    storyState.getIn(['story', 'data']),
  );

const makeSelectStoryError = () =>
  createSelector(selectStory, storyState =>
    storyState.getIn(['story', 'error']),
  );

const makeSelectWordData = () =>
  createSelector(selectStory, wordState => wordState.getIn(['word', 'data']));

const makeSelectWordError = () =>
  createSelector(selectStory, wordState => wordState.getIn(['word', 'error']));

export {
  makeSelectStoryData,
  makeSelectStoryError,
  makeSelectWordData,
  makeSelectWordError,
};
