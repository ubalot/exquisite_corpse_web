/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import PaperSheet from '../../components/Paper';
import { addWordToStory, fetchStoryData } from './actions';
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectStoryData,
  makeSelectStoryError,
  makeSelectWordError,
} from './selectors';
import StoryExposition from '../../components/StoryExposition';
import { makeSelectUserId } from '../App/selectors';
import Snackbar from '../../components/Snackbar';
import AddPhraseButton from '../../components/AddPhraseButton';

/* eslint-disable react/prefer-stateless-function */
class StoryPage extends React.PureComponent {
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    const { fetchStory } = this.props;
    const { storyId } = this.props.match.params;
    fetchStory(storyId);
  }

  render() {
    const {
      onAddWordToStory,
      currentUserId,
      story,
      storyError,
      wordError,
    } = this.props;

    return (
      <article>
        <Helmet>
          <title>Story Page</title>
          <meta name="description" content="Story view page" />
        </Helmet>
        <div>
          <PaperSheet>
            <StoryExposition
              storyTitle={
                story.title ||
                'So far so good, this story is empty for the moment...You should start it!'
              }
              words={story.words || []}
            />
            <AddPhraseButton
              key="add"
              storyId={story.id}
              userId={currentUserId}
              onSubmit={onAddWordToStory}
            />
          </PaperSheet>
        </div>
        {storyError && <Snackbar message={storyError} />}
        {wordError && <Snackbar message={wordError} />}
      </article>
    );
  }
}

StoryPage.propTypes = {
  match: PropTypes.object,
  story: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  fetchStory: PropTypes.func,
  onAddWordToStory: PropTypes.func,
  currentUserId: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  storyError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  wordError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

function mapDispatchToProps(dispatch) {
  return {
    fetchStory: storyId => dispatch(fetchStoryData(storyId)),
    onAddWordToStory: (word, userId, storyId) =>
      dispatch(addWordToStory(word, userId, storyId)),
  };
}

const mapStateToProps = createStructuredSelector({
  story: makeSelectStoryData(),
  currentUserId: makeSelectUserId(),
  storyError: makeSelectStoryError(),
  wordError: makeSelectWordError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'story', reducer });
const withSaga = injectSaga({ key: 'story', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(StoryPage);
