// export const SELECT_STORY = 'exquisite-corpse/StoryPage/SELECT_STORY';

export const FETCH_STORY_DATA_REQUEST =
  'exquisite-corpse/Story/FETCH_STORY_DATA_REQUEST';
export const FETCH_STORY_DATA_SUCCEDED =
  'exquisite-corpse/Story/FETCH_STORY_DATA_SUCCEDED';
export const FETCH_STORY_DATA_FAILED =
  'exquisite-corpse/Story/FETCH_STORY_DATA_FAILED';

export const ADD_WORD_TO_STORY_REQUEST =
  'exquisite-corpse/Story/ADD_WORD_TO_STORY_REQUEST';
export const ADD_WORD_TO_STORY_SUCCEDED =
  'exquisite-corpse/Story/ADD_WORD_TO_STORY_SUCCEDED';
export const ADD_WORD_TO_STORY_FAILED =
  'exquisite-corpse/Story/ADD_WORD_TO_STORY_FAILED';
