/*
 * StoryReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';
import {
  ADD_WORD_TO_STORY_SUCCEDED,
  ADD_WORD_TO_STORY_FAILED,
  FETCH_STORY_DATA_SUCCEDED,
  FETCH_STORY_DATA_FAILED,
} from './constants';

// The initial state of the App
export const initialState = fromJS({
  story: {
    data: false,
    error: false,
  },
  word: {
    data: false,
    error: false,
  },
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_STORY_DATA_SUCCEDED:
      return state
        .setIn(['story', 'data'], action.story)
        .setIn(['story', 'error'], false);
    case FETCH_STORY_DATA_FAILED:
      return state
        .setIn(['story', 'data'], false)
        .setIn(['story', 'error'], action.error);
    case ADD_WORD_TO_STORY_SUCCEDED:
      return state
        .setIn(['word', 'data'], action.word)
        .setIn(['word', 'error'], false);
    case ADD_WORD_TO_STORY_FAILED:
      return state
        .setIn(['word', 'data'], false)
        .setIn(['word', 'error'], action.error);
    default:
      return state;
  }
}

export default homeReducer;
