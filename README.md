# Exquisite Corpse (frontend)

A running app is already deployed to Heroku: [Exquisite Corpse](https://exquisite-corpse-web.herokuapp.com/)


## Running Locally

Make sure you have Node installed.  Also, install the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) (formerly known as the Heroku Toolbelt).

Clone code:
```sh
$ git clone https://gitlab.com/ubalot/exquisite_corpse_web.git
$ cd exquisite_corpse_web
```

Install project dependencies
```sh
$ npm install
```
<!-- $ bundle exec rake db:create db:migrate -->

To start the app run
```sh
$ heroku local
```

Your app should now be running on [localhost:3000](http://localhost:3000/).

## Deploying to Heroku

```sh
$ git push heroku master
$ heroku open
```
